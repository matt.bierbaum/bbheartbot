import os
import dill
import time
import random
from collections import defaultdict

import telebot
from telebot import types

SAVEFILE = './tally.pkl'
BUTTFILE = './tally-butts.pkl'
FACEFILE_GOOD = './faces-best.txt'
FACEFILE_BAD = './faces-bad.txt'
SECRETSFILE = './.secrets'


class Responder:
    def __init__(self):
        pass

    def istext(self, message):
        return message.content_type == 'text'

    def ismentioned(self, message):
        return (
            self.istext(message) and 
            ('@bbheartbot' in message.text or '@bb' in message.text)
        )

    def isreply(self, message):
        if message.reply_to_message:
            return True
        return False

    def user(self, message):
        return message.from_user.first_name

    def reply_user(self, message):
        return message.reply_to_message.from_user.first_name

    def chatid(self, message):
        return message.chat.id

    def text(self, message):
        return message.text


class NonResponder(Responder):
    def __init__(self):
        pass

    def respond(self, m):
        return


class FaceResponder(Responder):
    def __init__(self, facesall=FACEFILE_GOOD, facesbad=FACEFILE_BAD):
        self.facesall = facesall
        self.facesbad = facesbad

        self.gfaces = [i.strip() for i in open(self.facesall).readlines()]
        self.bfaces = [i.strip() for i in open(self.facesbad).readlines()]

    def respond(self, m):
        if not self.ismentioned(m):
            return

        if self.user(m) == 'Matt':
            return random.choice(self.bfaces)
        return random.choice(self.gfaces)


class HeartResponder(Responder):
    def __init__(self, savefile=SAVEFILE):
        self.msg = u'\u2665 \u2665 \u2665 {} \u2665 \u2665 \u2665'
        self.savefile = savefile

        if not os.path.exists(self.savefile):
            dill.dump(
                defaultdict(lambda: defaultdict(int)),
                open(self.savefile, 'wb')
            )

        self.tally = dill.load(open(self.savefile, 'rb'))

    def save(self):
        dill.dump(self.tally, open(self.savefile, 'wb'))

    def respond(self, m):
        if not self.ismentioned(m):
            return

        chatid = self.chatid(m)
        text = self.text(m)

        if self.isreply(m):
            user = self.user(m)
            op = self.reply_user(m)

            if user == op:
                self.tally[chatid][user] -= 1
                self.save()
                return u'tisk tisk {}, -1'.format(user)
            else:
                self.tally[chatid][op] += 1
                self.save()
                return self.msg.format(op)

        elif 'count' in text:
            return (
                u', '.join([
                    u'{}: {}'.format(k, v) for k,v in self.tally[chatid].items()
                ]) or 'No hearts yet'
            )

        elif 'help' in text:
            return (
                u'To interact with me, you can @bb or @bbheartbot \n'+
                u'  -- `count` tells the current heart counts \n'+
                u'  -- `help` gives this message\n' +
                u'  -- mentioning me in a reply hearts that reply\n'
            )


class ButtResponder(Responder):
    def __init__(self, savefile=BUTTFILE):
        self.savefile = savefile
        self.msg = u'\U0001F4A9 '*3 + '{}' + u'\U0001F4A9 '*3

    def ismentioned(self, m):
        return self.istext(m) and ('@bbbuttbot' in m.text or '@bu' in m.text)

    def respond(self, m):
        if not self.ismentioned(m):
            return

        if self.isreply(m):
            op = self.reply_user(m)
            return self.msg.format(op)


class BBHeartBot:
    def __init__(self, responders=None, default=None, secretsfile=SECRETSFILE):
        self.apikey = open(secretsfile).read().strip()
        self.responders = responders or []
        self.default_responder = default or NonResponder()

        self.bot = telebot.TeleBot(self.apikey)
        self.bot.set_update_listener(self.responder)

    def run(self):
        self.bot.polling()

    def responder(self, messages):
        for m in messages:
            reply = None

            for r in self.responders:
                reply = r.respond(m)
                if reply:
                    break

            if not reply:
                reply = self.default_responder.respond(m)

            if reply:
                self.bot.send_message(m.chat.id, reply)


while True:
    try:
        responders = [ButtResponder(), HeartResponder(), FaceResponder()]
        bot = BBHeartBot(responders=responders)
        bot.run()
    except Exception as e:
        print(e)
        time.sleep(10)
